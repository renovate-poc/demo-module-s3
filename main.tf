resource "aws_s3_bucket" "bucket" {
  bucket = var.NAME

  tags = {
    Name        = var.NAME
    Environment = "Dev"
  }
}