terraform {
  required_version = ">= 1.0"

  required_providers {
    # This is the minimal version of the provider(s) required by this module to work properly
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0"
    }
  }
}
